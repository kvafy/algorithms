
;; Find number of Employees Under every Employee

; http://www.geeksforgeeks.org/find-number-of-employees-under-every-manager/

; Given a dictionary that contains mapping of employee and his manager as a number
; of (employee, manager) pairs like below.

; { "A", "C" },
; { "B", "C" },
; { "C", "F" },
; { "D", "E" },
; { "E", "F" },
; { "F", "F" }

; F - C - A
;       - B
;   - E - D

; In this example C is manager of A. C is also manager of B. F is manager of C and so on.
; Write a function to get no of employees under each manager in the hierarchy not just their
; direct reports. It may be assumed that an employee directly reports to only one manager.
; In the above dictionary the root node/ceo is listed as reporting to himself.

; Output should be a Dictionary that contains following.

; A - 0
; B - 0
; C - 2
; D - 0
; E - 1
; F - 5


;; ------------------------------------------------------------------------------------------------


;; My solution #1
;; Build a child-parent map, walk from each node up the tree and increment counters for all visited nodes.
;; This is an O(n^2) algorithm unfortunatelly. Could compute sub-tree sizes recursively while using memoization.

(defn path-to-root [node child-parent-map]
  (loop [path (list)
         cur-node node]
    (let [parent (child-parent-map cur-node)]
      (if (and parent (not= cur-node parent))
        (recur (conj path parent) parent)
        path))))

(defn increase-counters [counters-map counters]
  (if (not-empty counters)
    (let [counter (first counters)
          counter-value (or (counters-map counter) 0)]
      (recur (assoc counters-map counter (inc counter-value))
             (next counters)))
    counters-map))

(defn no-of-subordinates-bottom-up [emp-mgmt-spec]
  (let [emp-mgmt-map (apply hash-map (apply concat emp-mgmt-spec))
        all-people (set (apply concat emp-mgmt-map))
        paths (map #(path-to-root % emp-mgmt-map) all-people)]
    (reduce increase-counters
            (apply hash-map (interleave all-people (repeat 0)))
            paths)))



;; My solution #2
;; Build a parent-children tree, recursively compute size of sub-tree of each node. Use memoization.
;; This is an O(n) algorithm at the price of higher memory consumption.

(defn subtree-size [tree node]
  (let [children (tree node)]
    (+ (count children)
       (reduce + (map #(subtree-size tree %) children)))))

(defn no-of-subordinates-up-to-bottom [emp-mgmt-spec]
  (let [emp-mgmt-spec (filter #(not= (first %) (second %)) emp-mgmt-spec)
        all-people (set (apply concat emp-mgmt-spec))
        mgmt-emps (reduce (fn [mgmt-emps [emp mgmt]]
                            (assoc mgmt-emps mgmt (conj (mgmt-emps mgmt) emp)))
                          (apply hash-map (interleave all-people (repeat #{})))
                          emp-mgmt-spec)
        memozed-subtree-size (memoize subtree-size)]
    (map #(hash-map % (memozed-subtree-size mgmt-emps %)) all-people)))


;; testing

(let [emp-mgmt-spec [ ["A" "C"] ["B" "C"] ["C" "F"] ["D" "E"] ["E" "F"] ["F" "F"] ] ]
  ;(no-of-subordinates-bottom-up emp-mgmt-spec)
  (no-of-subordinates-up-to-bottom emp-mgmt-spec)
  )
