;; Compute ceil(7n/8) without using multiplication or division.

(defn ceil-of-times7divide8 [x]
  (let [times7 (+ x x x x x x x)
        div8 (bit-shift-right times7 3)
        carry (if (zero? (bit-and x 7)) 0 1)]
    (+ div8 carry)))

(ceil-of-times7divide8 15)


;; Determine whether a number is a power of 2.

(defn power-of-2? [x]
  (and (pos? x)
       (zero? (bit-and x (dec x)))))

(power-of-2? -2)
(power-of-2? 0)
(power-of-2? 1)
(power-of-2? 5)
(power-of-2? 256)
