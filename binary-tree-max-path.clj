; Given a binary tree, find the maximum path sum. The path may start and end at any node in the tree.
; (not root-to-leaf, does not have to go through root at all, ...)

; http://www.geeksforgeeks.org/find-maximum-path-sum-in-a-binary-tree/


(defn max-path-in-tree [root]
  (letfn [; first, define helper functions that are safe with nils (ignore nil args and return nil iff all args are nil)
          (nil-reduce [f xs]
            (reduce (fn [curval x]
                      (cond
                       (nil? x)  curval
                       (nil? curval)  x
                       :else  (f curval x)))
                    nil
                    xs))
          (nil+ [& xs] (nil-reduce + xs))
          (nil-max [& xs] (nil-reduce max xs))
          (max-path [node]
            (cond
             (number? node)  [node node]
             (empty? node)  [nil nil]
             :else  (let [[val left right] node
                          [l-max-to-root l-max-any] (max-path left)
                          [r-max-to-root r-max-any] (max-path right)
                          max-to-root (+ val (nil-max l-max-to-root
                                                      r-max-to-root
                                                      0)) ; both left and right have only negative path => start over in 'node'
                          max-any (nil-max l-max-any
                                           r-max-any
                                           max-to-root
                                           (nil+ val l-max-to-root r-max-to-root))]
                      (vector max-to-root max-any))))]
    (apply max (max-path root))))


(max-path-in-tree [1  [2 [] []]
                      [3 [] []]])
; => 6


(max-path-in-tree [10  [2  [20  [] []]
                           [1   [] []]]
                       [10 [-25 [3 [] []]
                                [4 [] []]]]])
; => 42
