; http://www.geeksforgeeks.org/bankbazaar-interview-experience-set-3/

; Consider an array: [1,2,5,6,5,2]
; Get the index of the element which is at-least repeated once & has the lowest index.
; Output for the above given array is: 2

(defn lowest-index-of-repeated-item [coll]
  (let [item-index-tuples (partition 2 (interleave coll (range)))
        item-to-last-index (apply hash-map (apply concat item-index-tuples))
        duplicated-item-index-tuples (filter (fn [[item idx]]
                                               (not= idx (item-to-last-index item)))
                                             item-index-tuples)]
    (if (empty? duplicated-item-index-tuples)
      -1
      (second (first duplicated-item-index-tuples)))))

(lowest-index-of-repeated-item [1 2 5 6 5 2])



; http://www.geeksforgeeks.org/largest-rectangle-under-histogram/
; http://www.informatik.uni-ulm.de/acm/Locals/2003/html/histogram.html

; Find the largest rectangular area possible in a given histogram where the largest rectangle can be made
; of a number of contiguous bars. For simplicity, assume that all bars have same width and the width is 1 unit.

; For example, consider the following histogram with 7 bars of heights {6, 2, 5, 4, 5, 1, 6}.
; The largest possible rectangle possible is 12

; Algorithm:
;   initialize: stack = (), curmax = 0
;   for each bar left to right (i-th is current bar, bars indexed from 0):
;       while stack not empty and array[top(stack)] > array[i]:
;           height = array[top(stack)]
;           pop(stack)
;           width = if stack has at least one more item THEN (i - (top(stack) + 1)) ELSE i
;           curmax = max(curmax, height*width)
;       push(stack, i)
;   while stack not empty:
;       curmax = <same as above, including the popping>
;   return curmax
;
; Principle:
;   The stack holds indexes of bars such that bar heights on those indexes are non-decreasing.
;   Thereby we keep track of changes of rectangle heights to the left of the current position.
;   If a lower bar is encountered, some index(es) from the stack need to be popped so that the
;   non-decreasing-bars invariant holds true; while popping, we check whether the formed rectangles
;   aren't the biggest seen so far.

(defn max-rect-in-histogram
  ([bars]
    (max-rect-in-histogram bars [0 () 0]))
  ([bars [i stack curmax]]
    (if (and (not-empty stack)
             (or (= i (count bars))
                 (> (nth bars (first stack)) (nth bars i))))
      ; pop from the stack (either the current bar is lower than the one on top of stack OR all bars were processed)
      (let [; top of the stack determines height of the currently processed rectangle
            height (nth bars (first stack))
            ; the width is tricky: if the stack contains at least two elements, then then
            ; second element +1 denotes start of the rectangle; if stack has no more elements
            ; then we know that when the current top item on stack was pushed in, the stack contained
            ; indexes of bars all higher than the index on top of stack, therefore
            ; we take the whole "i" as width of the rectangle because every bar on the left
            ; is at least as high as the stack(top)-th bar
            width (if (>= (count stack) 2) (- i (+ (fnext stack) 1)) i)]
        (recur bars [i (next stack) (max curmax (* width height))]))
      ; don't pop, either process next bar or quit the computation
      (if (= i (count bars))
        curmax
        (recur bars [(inc i) (conj stack i) curmax])))))

(max-rect-in-histogram [6 2 5 4 5 1 6])



; Find the maximum all-ones rectangular area in a binary matrix.
; This is solved easily once we can find the largest rectangle in a histogram. With each row
; we simply update the current histogram and find the maximum rectangle in this histogram.

(defn max-1s-rect-in-binary-matrix
  ([matrix]
    (let [column-size (count (first matrix))]
      (max-1s-rect-in-binary-matrix matrix [(repeat column-size 0) 0])))
  ([matrix [histogram-prev curmax]]
    (if (empty? matrix)
      curmax
      (let [histogram (map (fn [bar-prev value]
                             (if (= 1 value) (+ bar-prev 1) 0))
                           histogram-prev
                           (first matrix))]
        (recur (next matrix) [histogram (max curmax (max-rect-in-histogram histogram))])))))

(max-1s-rect-in-binary-matrix [[0  1  1  0  1]
                               [1  1  0  1  0]
                               [0  1  1  1  0]
                               [1  1  1  1  0]
                               [1  1  1  1  1]
                               [0  0  0  0  0]])



; Given an array of distinct elements, rearrange the elements of array in zig-zag fashion in O(n) time.
; The converted array should be in form a < b > c < d > e < f.
; http://www.geeksforgeeks.org/convert-array-into-zig-zag-fashion/

(defn zig-zag
  ([arr]
    (zig-zag arr (apply concat (repeat (list < >)))))
  ([arr [rel & rels]]
    (if (>= 1 (count arr))
      arr
      (let [[x y & zs] arr]
        (if (rel x y)
          (cons x (zig-zag (cons y zs) rels))
          (cons y (zig-zag (cons x zs) rels)))))))

(zig-zag (range 5))
