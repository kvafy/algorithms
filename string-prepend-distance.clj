; Given two strings A and B, the task is to convert A to B if possible.
; The only operation allowed is to put any character from A and insert it at front.
; Find if it’s possible to convert the string. If yes, then output minimum no. of operations required for transformation.

; Input:  A = "ABD", B = "BAD"
; Output: 1
; Explanation: Pick B and insert it at front.

; Input:  A = "EACBD", B = "EABCD"
; Output: 3
; Explanation: Pick B and insert at front, EACBD => BEACD
;             Pick A and insert at front, BEACD => ABECD
;             Pick E and insert at front, ABECD => EABCD

(defn prepend-distance [orig target]
  ; Algorithm: Match orig and target from the end to beginning, one character at a time.
  ;            If the last character matches, compute the distance recursively for prefixes of the strings.
  ;            Otherwise transfer the last character of orig into 'picks' bag, increase counter of changes and
  ;            compute recursively. The 'picks' bag holds letters that will be prepended to orig in an order
  ;            that is unknown at this point.
  ; Note: Using a vector for 'picks' results in O(n^2) time complexity. Better to use some multiset implementation instead.
  (letfn [(remove [v x]
            (cond (empty? v) v
                  (= (last v) x) (butlast v)
                  :else (conj (remove (butlast v) x) (last v))))]
    (loop [orig   orig
           target target
           cnt    0
           picks  []]
      (cond
       (empty? target) (if (every? empty? [orig picks]) cnt -1)
       (empty? orig) (if (some (partial = (last target)) picks)
                       (recur orig (butlast target) cnt (remove picks (last target)))
                       -1)
       :else (let [lorig (last orig)
                   ltarget (last target)]
               (if (= lorig ltarget)
                 (recur (butlast orig) (butlast target) cnt picks)
                 (recur (butlast orig) target (inc cnt) (conj picks (last orig)))))))))

(prepend-distance "ABD" "BAD")
; => 1
(prepend-distance "EACBD" "EABCD")
; => 3
(prepend-distance "A" "B")
; => -1
