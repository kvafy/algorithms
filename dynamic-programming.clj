
;; Given an input number of sections, find all possible ways to construct buildings in the sections such that there is a space between any 2 buildings.

; notation:
;   emp(n) - number of combinations how to use n sections and have the n-th section empty
;   bld(n) - number of combinations how to use n sections and have the n-th section occupied by a building
; computation:
;   emp(1) = 1
;   bld(1) = 1
;   bld(i) = emp(i-1)
;   emp(i) = emp(i-1) + bld(i-1)

(defn construct-buildings [n]
  (let [emp-bld-tuples (iterate (fn [[emp-i-1 bld-i-1]]
                                    [(+ emp-i-1 bld-i-1) emp-i-1])
                                [1N 1N])]
    (apply + (nth emp-bld-tuples (dec n)))))

(construct-buildings 4)



;; Given an input string and a dictionary of words, find out if the input string can be segmented into a space-separated sequence of dictionary words.

; notation:
;   coverable(n) - true if sentence up to n-th character is coverable by words from dictionary
; computation:
;   coverable(0) = true
;   coverable(i) = true  <=>  exists word: (word is suffix of sentence(0,i)) and (coverable(i - length(word)))

; note: the suffix checking could be made faster by constructing a trie containing reversed words

(defn word-breakdown [dict sentence]
  (iterate (fn [coverable]
             (let [i (count coverable)
                   subsentence (subs sentence 0 i)
                   can-be-suffixed (some #(and (.endsWith subsentence %)
                                               (<= (count %) i)
                                               (nth coverable (- i (count %))))
                                         dict)]
               (conj coverable (boolean can-be-suffixed))))
           [true]))

(let [dict ["i" "like" "sam" "sung" "samsung" "mobile" "ice" "cream" "icecream" "man" "go" "mango"]
      sentence "ilikesamgomanmobile"]
  (nth (word-breakdown dict sentence)
       (count sentence)))



;; Edit distance of two strings

; notation:
;   ed(i,j) = best achievable edit distance for strings s1(0,i) and s2(0,j)
; computation
;  ed(0,j) = j
;  ed(i,0) = i
;  ed(i,j) = min( 1 + ed(i-1,j), 1 + ed(i,j-1), ed(i-1,j-1) + delta(s1(i-1),s2(j-1)) )

; :( this version is memoized but stack overflow may occur

(def edit-distance
  (memoize (fn ([s1 s2]
                 (edit-distance s1 s2 (count s1) (count s2)))
               ([s1 s2 i j]
                 (cond
                  (zero? i) j
                  (zero? j) i
                  :else (min (+ 1 (edit-distance s1 s2 (dec i) j))
                             (+ 1 (edit-distance s1 s2 i (dec j)))
                             (+ (edit-distance s1 s2 (dec i) (dec j))
                                (if (= (nth s1 (dec i)) (nth s2 (dec j))) 0 1))))))))

(edit-distance "banana" "banana")




;; Fastest possible notification of all nodes in a tree.
;; There is a tree (supervisor-subordinate), a node can notify only one child at a time and
;; can notify only direct children, not transitive ones. How many rounds/iterations will it
;; take at least until the whole tree is notified?

; https://cgi.csc.liv.ac.uk/~martin/teaching/comp202/Exercises/Dynamic-programming-exercises-solution.pdf (excercise 5)

(defn tree-notification-delay [g node]
  (let [children (g node)]
    (if (empty? children)
      0
      (apply max (->> children
                      (map #(tree-notification-delay g %))
                      ; can prove by exchange argument that such ordering (the following two lines) is optimal
                      (sort-by #(- 0 %))
                      (map vector (next (range)))
                      (map #(apply + %)))))))

(let [tree {:a #{:b :d}
            :b #{:c}}
      root :a]
  (tree-notification-delay tree root))




;; Longest increasing subsequence.
;; Find length of the longest increasing subsequence of items (not necessarily following right after each other)
;; so that they form an increasing sequence.

;; notation:
;;   L(i) = length of the longest increasing subsequence of items ending at and including i-th item.
;; computation:
;;   L(0) = 1
;;   L(i) = 1 + max {L(j) | j < i and val(j) < val(i)}
(defn longest-increasing-subseq
  ([values]
    (apply max (longest-increasing-subseq values [])))
  ([values L]
    (if (= (count L) (count values))
      L
      (let [i (count L)
            Ljs (->> (range i)
                     (filter #(< (nth values %) (nth values i)))
                     (map #(nth L %)))
            maxLj (if-not (empty? Ljs) (apply max Ljs) 0)]
        (recur values (conj L (inc maxLj)))))))

(longest-increasing-subseq (interleave (range 10) (repeat -5)))
